# Overview

This repository is a `RepoSync`  repoistory that deomonstrates how to isolate a team, vendor/partner and/or location into a grouped "Namespace" repository. In this example
all of the folders are "locations" but easily could be applications by a vendor/partner or some other isolated and grouped KRM (Kubernetes Resources Model) config.

## Folders

All of the grouped locations are under the `/config` folder by convention

## Adding A Location to your Primary Root Repository

```yaml
apiVersion: configsync.gke.io/v1beta1
kind: RepoSync
metadata:
  name: <rs-name> # keep this short
  namespace: <intended-namespace>
spec:
  sourceFormat: "unstructured"
  git:
    repo: "https://gitlab.com/gcp-solutions-public/retail-edge/location-repo-1.git"
    branch: "main"
    dir: "/config/<location-folder>"            # Folder inside the repository corresponding to resources to use in cluster
    auth: "token"
    secretRef:
      name: locations-git-creds                 # K8s secret produced by ExternalSecret below

---

apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: locations-git-creds-es
  namespace: <intended-namespace>
spec:
  refreshInterval: 1m
  secretStoreRef:
    kind: ClusterSecretStore
    name: gcp-secret-store
  target:                                       # K8s secret definition
    name: locations-git-creds                   ############# Matches the secretRef above
    creationPolicy: Owner
  data:
  - secretKey: username                         # K8s secret key name inside secret
    remoteRef:
      key: locations-access-token-creds         #  GCP Secret Name
      property: username                        # field inside GCP Secret
  - secretKey: token                            # K8s secret key name inside secret
    remoteRef:
      key: locations-access-token-creds         #  GCP Secret Name
      property: token                           # field inside GCP Secret

---

 kind: RoleBinding
 apiVersion: rbac.authorization.k8s.io/v1
 metadata:
   name: syncs-repo
   namespace: <intended-namespace>
 subjects:
 - kind: ServiceAccount
   name: ns-reconciler-<intended-namespace>-<rs-name>-<length-of-rs-name>
   namespace: config-management-system
 roleRef:
   kind: ClusterRole
   name:  admin                                 # Granular control over resources can be created with RBAC
   apiGroup: rbac.authorization.k8s.io

```
### Create GCP Secret for git-creds

Create the GCP Secret Manager secret used by `ExternalSecret` to proxy for K8s `Secret`

```
export PROJECT_ID=<your google project id>
export SCM_TOKEN_TOKEN=<your gitlab personal-access token value>
export SCM TOKEN_USER=<your gitlab personal-access token user>

gcloud secrets create locations-access-token-creds --replication-policy="automatic" --project="${PROJECT_ID}"
echo -n "{\"token\":\"${SCM_TOKEN_TOKEN}\", \"username\":\"${SCM_TOKEN_USER}\"}"  | gcloud secrets versions add locations-access-token-creds --project="${PROJECT_ID}" --data-file=-
```


## Local Validation

Assuming `nomos` is installed (via `gcloud components install nomos`)

```
nomos vet --no-api-server-check --path config/
```

### Docker method

Using this link to find the version of nomos-docker:  https://cloud.google.com/anthos-config-management/docs/how-to/updating-private-registry#expandable-1

```
docker pull gcr.io/config-management-release/nomos:stable
docker run -it -v $(pwd):/code/ gcr.io/config-management-release/nomos:stable nomos vet --no-api-server-check --path /code/config/
```

### ACM Overview

See [our documentation](https://cloud.google.com/anthos-config-management/docs/repo) for how to configure and use Repositories w/ ConfigSync.
